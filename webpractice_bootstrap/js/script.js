const offerFullImage = document.querySelector('.offer__full-img img');

addOfferButtonsClickEvent();

function addOfferButtonsClickEvent() {
    const offerButtons = document.querySelector(".offer__buttons");

    if (offerButtons) {
        offerButtons.addEventListener('click', offerButtonsClickEvent)
    }
}

function offerButtonsClickEvent(evt) {
    const target = evt.target.closest('button');
    
    if (!target) {
        return
    }
    
    offerFullImage.src = target.dataset.img;
}